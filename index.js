function show(){
    class even_odd{
        constructor(number){
            this.number=number;
        }
        even(){
            Number=this.number;
            var evenNumbers=" ";
            for(let i=0;i<Number.length;i++){
                if(Number[i]%2 == 0){
                    if(evenNumbers.includes(Number[i])){
                        continue;
                    }
                    else{
                        evenNumbers += Number[i] + " , ";
                    }
                }
            }
            this.evenNumbers = evenNumbers;
        }

        odd(){
            Number=this.number;
            var oddNumbers=" ";
            for(let i=0;i<Number.length;i++){
                if(Number[i]%2 != 0){
                    if(oddNumbers.includes(Number[i])){
                        continue;
                    }
                    else{
                        oddNumbers += Number[i] + " , ";
                    }
                }
            }
            this.oddNumbers = oddNumbers;
        }
        display(){
            document.getElementById('output').innerHTML="EVEN : "+ this.evenNumbers +"<br>ODD : " +this.oddNumbers; 
        }
    }

    let result = new even_odd(`${document.getElementById("input").value}`)
    result.even();
    result.odd();
    result.display();
    
}